import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { getDeviceLang, getCorrectIcon } from "../../utils";

class DashboardListItems extends Component {
  render() {
    const { item, goDetails, language } = this.props;

    return (
      <TouchableOpacity onPress={() => goDetails(item.meterId, item.address)}>
        <View
          style={[
            styles.wrap,
            getDeviceLang(language) === "Left" ? styles.langE : styles.langH,
          ]}
        >
          {getDeviceLang(language) === "Left" ? (
            <>
              <View>
                <Image source={getCorrectIcon()} style={styles.image} />
              </View>
              <View style={styles.wrapText}>
                <Text style={styles.address}>{item.address}</Text>
                <Text style={styles.meter}>{item.number}</Text>
              </View>
            </>
          ) : (
            <>
              <View style={styles.wrapText}>
                <Text style={styles.address}>{item.address}</Text>
                <Text style={styles.meter}>{item.number}</Text>
              </View>
              <View>
                <Image source={getCorrectIcon()} style={styles.image} />
              </View>
            </>
          )}
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  wrap: {
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
    height: 100,
    borderColor: "rgba(0,0,0, .1)",
    borderWidth: 1,
    borderStyle: "solid",
    borderRadius: 5,
    marginTop: 15,
    paddingLeft: 15,
  },
  langE: {
    justifyContent: "flex-start",
  },
  langH: {
    justifyContent: "flex-end",
    paddingRight: 15,
  },
  image: {
    alignSelf: "center",
    width: 55,
    height: 55,
  },
  wrapText: {
    justifyContent: "center",
    marginLeft: 20,
    marginRight: 20,
  },
  address: {
    fontSize: 18,
  },
  meter: {
    color: "grey",
  },
});

const mapStateToProps = (state) => ({
  language: state.accountReducer.language,
});
const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(DashboardListItems);
