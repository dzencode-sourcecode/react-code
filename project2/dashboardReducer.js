import { Alert } from "react-native";
import i18next from "i18next";

import { cityGridApi } from "../../api/index";
import { setLoading } from "./accountReducer";

const initialState = {
  recentlyReports: [],
  dashboardElements: [],
  meters: [],
  currentMeter: {},
  statisticData: [],
  lastMeterValue: "",
  LastMeterDate: "",
  currentMonthConsumption: "",
};

const dashboardReducer = (state = initialState, action) => {
  switch (action.type) {
    //dashboard
    case "GET_DASHBOARD_DETAILS":
      return {
        ...state,
        dashboardElements: action.payload,
        meters: action.payload.map((item) => item.meters).flat(),
      };
    case "SET_RESENTLY_REPORTS":
      return {
        ...state,
        recentlyReports: action.payload,
      };
    case "SET_CURRENT_METER":
      return {
        ...state,
        currentMeter: action.payload.meter,
        lastMeterValue: action.payload.value,
        LastMeterDate: action.payload.date,
        currentMonthConsumption: action.payload.current,
      };
    case "SET_STATISTIC_DATA":
      return {
        ...state,
        statisticData: action.payload,
      };

    default:
      return state;
  }
};

//---------------actions

export const setDashboardDetails = (details) => {
  return {
    type: "GET_DASHBOARD_DETAILS",
    payload: details,
  };
};

export const setCurrentMeter = (meter, current, date, value) => ({
  type: "SET_CURRENT_METER",
  payload: { meter, current, date, value },
});

export const setRecentlyReport = (items) => ({
  type: "SET_RESENTLY_REPORTS",
  payload: items,
});
export const setStatisticData = (items) => ({
  type: "SET_STATISTIC_DATA",
  payload: items,
});

//----------------thunks

export const getDashboardDetails = () => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await cityGridApi.getDashboardElem();
    dispatch(setDashboardDetails(response.data.dashboardElem));
    dispatch(setLoading(false));
  } catch (error) {
    dispatch(setLoading(false));
    Alert.alert(i18next.t("Something wrong with connection, try again later"));
  }
};

export const getCurrentMeterDetails = (meter) => async (dispatch) => {
  try {
    if (meter.type === (1 || 3)) {
      dispatch(setLoading(true));
      const response = await cityGridApi.getElectricityRecentStatistic(
        meter.meterId
      );
      dispatch(
        setCurrentMeter(
          meter,
          response.data.consumptionCurrentMonth,
          response.data.lastReadingDate,
          response.data.lastReading
        )
      );
      dispatch(setLoading(false));
    } else {
      dispatch(setLoading(true));
      const response = await cityGridApi.getWaterOrGazRecentStatistic(
        meter.meterId
      );
      dispatch(
        setCurrentMeter(
          meter,
          response.data.consumptionCurrentMonth,
          response.data.lastReadingDate,
          response.data.lastReading
        )
      );
      dispatch(setLoading(false));
    }
  } catch (error) {
    dispatch(setCurrentMeter({}, null, null, null));
    dispatch(setLoading(false));
    Alert.alert(i18next.t("Something wrong with connection, try again later"));
  }
};

export const getRecentlyReport = (id) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await cityGridApi.getRecentlyReports(id);
    dispatch(setRecentlyReport(response.data));
    dispatch(setLoading(false));
  } catch (error) {
    dispatch(setLoading(false));
    Alert.alert("Something wrong with connection, try again later");
  }
};

export const getStatisticData = (id) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await cityGridApi.getMonthlyStatistic(id);
    dispatch(setStatisticData(response.data));
    dispatch(setLoading(false));
  } catch (error) {
    dispatch(setLoading(false));
    Alert.alert(i18next.t("Something wrong with connection, try again later"));
  }
};

export default dashboardReducer;
