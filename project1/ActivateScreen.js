import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  Platform,
  Alert,
  KeyboardAvoidingView,
  TouchableOpacity,
} from "react-native";

import {
  InputGroup,
  Container,
  Icon,
  Content,
  Input,
  List,
  ListItem,
} from "native-base";

import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { useSelector, useDispatch } from "react-redux";

import * as keyActions from "../../store/actions/key";
import { readableDate } from "../../utils/timer";
import HeaderButton from "../../components/UI/HeaderButton";
import ProItem from "../../components/UI/ProItem";

const ActivateScreen = (props) => {
  const [error, setError] = useState();
  const [coupon, setCoupon] = useState("");
  const isPro = useSelector((state) => state.key.isPro);

  const dispatch = useDispatch();

  useEffect(() => {
    if (error && error.length > 0) {
      return Alert.alert("An error occurred!", error, [{ text: "Okay" }]);
    }
  }, [error]);

  useEffect(() => {
    dispatch(keyActions.checkPro());
    return () => {
      dispatch(keyActions.checkPro());
    };
  }, []);

  const submitHandler = async () => {
    try {
      dispatch(keyActions.setProAccount(coupon, isPro));
    } catch (err) {
      setError(err.message);
    }
    setCoupon("");
  };

  const contentList = () => {
    return (
      <List style={styles.container}>
        <ListItem style={styles.list}>
          <InputGroup>
            <Icon
              name="ios-key"
              style={{ transform: [{ rotate: "180deg" }] }}
            />
            <Input
              className="grey"
              placeholderTextColor="#cccccc"
              placeholder="ВВЕДИТЕ КОД АКТИВАЦИИ"
              id="coupon"
              label="coupon"
              value={coupon}
              keyboardType="default"
              autoCorrect
              returnKeyType="next"
              required
              multiline
              onChangeText={(text) => setCoupon(text)}
              numberOfLines={4}
              maxLength={150}
            />
          </InputGroup>
        </ListItem>

        <ListItem style={styles.righted}>
          <Text style={styles.text}>Количество символов - {coupon.length}</Text>
        </ListItem>

        <View style={styles.centered}>
          <Text style={styles.text}>
            {isPro.isActive
              ? `Pro версия активна до - ${readableDate(
                  new Date(isPro.expiredTime)
                )}`
              : "Активируйте Pro версию"}
          </Text>
        </View>
      </List>
    );
  };

  return (
    <Container style={{ backgroundColor: "#fff", padding: 10 }}>
      <Content>
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          behavior="padding"
          keyboardVerticalOffset={100}
        >
          {contentList()}
        </KeyboardAvoidingView>
        <View style={{ alignItems: "center", justifyContent: "center" }}>
          <TouchableOpacity
            onPress={submitHandler}
            style={styles.submitButton}
            disabled={!coupon.length}
          >
            <Text style={styles.textChangeButton}>АКТИВАЦИЯ</Text>
          </TouchableOpacity>
        </View>
      </Content>
    </Container>
  );
};
ActivateScreen.navigationOptions = (navData) => {
  console.log("NAVIGATION", navData.navigation);
  return {
    headerTitle: "Активировать Pro",
    headerLeft: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu"
          iconName={Platform.OS === "android" ? "md-menu" : "ios-menu"}
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    ),
    headerRight: <ProItem />,
  };
};

const styles = StyleSheet.create({
  form: {
    margin: 20,
  },
  centered: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  container: {
    display: "flex",
    marginTop: 40,
  },
  righted: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    width: "90%",
    borderColor: "transparent",
    marginTop: -7,
    marginBottom: 30,
  },
  list: {
    borderWidth: 2,
    borderRadius: 5,
    width: "80%",
    display: "flex",
    alignSelf: "center",
  },
  text: {
    color: "grey",
    padding: 0,
    fontSize: 16,
    fontFamily: "Roboto",
    fontWeight: "400",
    lineHeight: 16,
    letterSpacing: 0.00938,
  },
  submitButton: {
    alignItems: "center",
  },
  actions: {
    flexDirection: "column",
    justifyContent: "center",
  },
  textChangeButton: {
    backgroundColor: "#3f51b5",
    color: "#fff",
    textAlign: "center",
    padding: 10,
    borderRadius: 5,
    margin: 10,
  },
  textChangeButton2: {
    backgroundColor: "#b53f51",
    color: "#fff",
    textAlign: "center",
    padding: 10,
    margin: 10,
    borderRadius: 5,
  },
});

export default ActivateScreen;
