import React, { useState, useEffect } from "react";
import { createStore, combineReducers, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import ReduxThunk from "redux-thunk";
import { useSelector, useDispatch } from "react-redux";
import socketIo from "socket.io-client";
import { sha256 } from "js-sha256";
import { AppLoading } from "expo";
import * as Font from "expo-font";
import * as messagesActions from "./store/actions/messages";
import { SOCKET_URL } from "./utils/constants";
import boxesReducer from "./store/reducers/boxes";
import keyReducer from "./store/reducers/key";
import messagesReducer from "./store/reducers/messages";
import BoxNavigator from "./navigation/BoxNavigator";

const rootReducer = combineReducers({
  boxes: boxesReducer,
  key: keyReducer,
  messages: messagesReducer,
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

const fetchFonts = () => {
  return Font.loadAsync({
    "open-sans": require("./assets/fonts/OpenSans-Regular.ttf"),
    "open-sans-bold": require("./assets/fonts/OpenSans-Bold.ttf"),
    Roboto: require("native-base/Fonts/Roboto.ttf"),
    Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
  });
};

const AppContainer = (props) => {
  const io = socketIo(SOCKET_URL.socket);
  io.on("connect", () => {
    console.log("<========socket connected");
  });
  return (
    <Provider store={store}>
      <App io={io} />
    </Provider>
  );
};

const App = ({ io }) => {
  const dispatch = useDispatch();
  const [fontLoaded, setFontLoaded] = useState(false);
  const isPro = useSelector((state) => state.key.isPro);
  let messages = useSelector((state) => state.messages.messages);
  messages = isPro.isActive ? messages : messages.slice(0, 2);

  useEffect(async () => {
    await dispatch(messagesActions.fetchMessages());
  }, []);

  useEffect(async () => {
    const keysList = messages.map((item) => sha256(item.key));
    await io.emit("keysList", keysList);
  }, [messages]);

  useEffect(async () => {
    await io.on("notificationUpdate", (hash) => {
      dispatch(messagesActions.addNotification(hash));
    });
  }, []);

  if (!fontLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => {
          setFontLoaded(true);
        }}
      />
    );
  }
  return <BoxNavigator />;
};

export default AppContainer;
