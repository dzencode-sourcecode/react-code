import { takeEvery, put, call, select, delay } from "redux-saga/effects";
import {
  LOAD_CURRENT_PAYOUT,
  LOAD_NEXT_PAYOUT,
  putCurrentPayout,
  putFuturePayout,
} from "./action";
import api from "../../config/api";
import { apiConfig } from "../../config/config";
import * as RootNavigation from "../../modules/RootNavigation";
import {
  showLoadingIndicator,
  hideLoadingIndicator,
} from "../../store/actions/loadingIndicator";
import routes from "../../modules/routes";
import { ERROR_MESSAGE } from "../../components/constants";

const getUser = (state) => state.sellerLoginReducer;

async function fetchCurrentData(user) {
  return fetch(api.balanceNextPayouts(user.user_id, "EN"), apiConfig)
    .then((response) => response.json())
    .catch((error) => {
      put({ type: "SHOW_NOTIFICATION", success: false, text: ERROR_MESSAGE });
      delay(3000);
      put({ type: "HIDE_NOTIFICATION" });
    });
}

async function fetchFutureData(user) {
  return fetch(api.balanceFuturePayouts(user.user_id, "EN"), apiConfig)
    .then((response) => response.json())
    .catch((error) => {
      put({ type: "SHOW_NOTIFICATION", success: false, text: ERROR_MESSAGE });
      delay(3000);
      put({ type: "HIDE_NOTIFICATION" });
    });
}

function* workerLoadNextBalance() {
  const user = yield select(getUser);
  yield put(showLoadingIndicator());
  const data = yield call(fetchCurrentData, user);
  yield put(putCurrentPayout(data));
  yield put(hideLoadingIndicator());
  yield call(() => RootNavigation.navigate(routes.CurrentPayout));
}

function* workerLoadFutureBalance() {
  const user = yield select(getUser);
  yield put(showLoadingIndicator());
  const data = yield call(fetchFutureData, user);
  yield put(putFuturePayout(data));
  yield put(hideLoadingIndicator());
  yield call(() => RootNavigation.navigate(routes.FuturePayout));
}

export function* watchLoadCurrentBalance() {
  yield takeEvery(LOAD_CURRENT_PAYOUT, workerLoadNextBalance);
}

export function* watchLoadFutureBalance() {
  yield takeEvery(LOAD_FUTURE_PAYOUT, workerLoadFutureBalance);
}
