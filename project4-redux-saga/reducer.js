import {
  PUT_CURRENT_PAYOUT,
  PUT_FUTURE_PAYOUT,
} from "../../actions/sell/balanceFuturePeriods";

const initialState = {
  nextPayOts: [],
  futurePayOts: [],
};

export const balanceReducer = (state = initialState, action) => {
  switch (action.type) {
    case PUT_CURRENT_PAYOUT:
      return { ...state, nextPayOts: action.nextPayout };
    case PUT_FUTURE_PAYOUT:
      return { ...state, futurePayOts: action.futurePayout };
    default:
      return state;
  }
};
