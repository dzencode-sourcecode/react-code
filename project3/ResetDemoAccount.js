import React, { useState, useEffect } from "react";
import { connect } from "react-redux";

import {
  Animated,
  View,
  Text,
  TouchableOpacity,
  TextInput,
} from "react-native";

import I18n from "../../../../utils/i18n";
import { Row, Select } from "../../../components/common/index";

import SocketService from "../../../../utils/socketService";
import { ThemeContext } from "../../../../utils/themeProvider";
import * as TerminalOperations from "../../../../state/terminal/operations";

import {
  commonStyles,
  typography,
  COLOR_CONSTANT,
  buttonStyles,
} from "../../../stylesheets";
import authStyles from "../../../stylesheets/auth";

const mapStateToProps = ({
  settingsState: { accountCurrency, accountLeverage },
}) => ({
  accountCurrency,
  accountLeverage,
});

const mapDispatchToProps = {
  clearOrdersList: TerminalOperations.clearOrdersList,
};

const ResetDemoAccount = ({
  accountCurrency = [{}],
  accountLeverage = [],
  navigation,
  clearOrdersList,
}) => {
  const [balance, setBalance] = useState(
    accountCurrency[0].default_balance || 0
  );
  const [balanceError, setBalanceError] = useState("");

  const [apiError, setApiError] = useState("");

  const [currency, setCurrency] = useState(accountCurrency[0].id);

  const [minMaxBalance, setMinMaxBalance] = useState([
    accountCurrency[0].min,
    accountCurrency[0].max,
  ]);

  const [leverage, setLeverage] = useState(
    (accountLeverage.find((lev) => lev.is_default) || {}).id
  );

  useEffect(() => {
    if (balance < minMaxBalance[0]) {
      setBalanceError(`Min balance is ${minMaxBalance[0]}:`);
    } else if (balance > minMaxBalance[1]) {
      setBalanceError(`Max balance is ${minMaxBalance[1]}:`);
    } else {
      setBalanceError("");
    }
  }, [balance, minMaxBalance]);

  useEffect(() => {
    const { min, max } =
      accountCurrency.find((item) => item.id === currency) || {};
    setMinMaxBalance([min, max]);
  }, [currency]);

  const handleSubmit = async () => {
    setApiError("");

    const activeTerminal = navigation.getParam("activeTerminal");
    const { response } = await SocketService.asyncGet(
      `/platform/resetDemoAccount?id=${activeTerminal}&currency_id=${currency}&leverage_id=${leverage}&balance=${balance}&account_id=${activeTerminal}`
    );

    if (response && response.statusCode === 200) {
      clearOrdersList();
      navigation.goBack();
    } else {
      setApiError((response.body && response.body.message) || "Error");
    }
  };

  const currentCurrency = accountCurrency[0];

  const isSubmitDisabled = () =>
    balanceError || !balance || !currency || !leverage;

  return (
    <ThemeContext.Consumer>
      {({ theme }) => (
        <Animated.ScrollView
          contentContainerStyle={{ flexGrow: 1 }}
          style={[
            authStyles.container,
            { backgroundColor: theme.mainBackground },
          ]}
        >
          <View style={authStyles.wrapper}>
            <View>
              <Text
                style={[
                  typography.h1,
                  {
                    color: theme.mainColor,
                  },
                ]}
              >
                {I18n.t("Reset Demo Account")}
              </Text>
              <Text
                style={[
                  typography.mediumText,
                  {
                    marginVertical: 8,
                    color: theme.secondColor,
                  },
                ]}
              >
                {I18n.t(
                  "This function will reset your demo account according to your individual settings. Note that after you reset your demo account all the accumulated trading history will be deleted, and trading systems in your portfolio disconnected (you will need to connect them again)."
                )}
              </Text>
              {balanceError ? (
                <Text
                  style={[authStyles.label, { color: COLOR_CONSTANT.darkred }]}
                >
                  {I18n.t(balanceError)}
                </Text>
              ) : (
                <Text style={[authStyles.label, { color: theme.secondColor }]}>
                  {I18n.t("Initial balance")}
                </Text>
              )}
              <View>
                <TextInput
                  keyboardType="numeric"
                  style={[
                    commonStyles.textInput,
                    {
                      backgroundColor: theme.secondBackground_01,
                      color: theme.mainColor,
                    },
                  ]}
                  onChangeText={(text) => {
                    if (parseInt(text, 10)) setBalance(parseInt(text, 10));
                    else setBalance(0);
                  }}
                  value={balance.toString()}
                />
                <TouchableOpacity
                  onPress={() => {
                    if (balance > currentCurrency.min) {
                      setBalance(balance - 500);
                    }
                  }}
                  style={[
                    commonStyles.centeredContent,
                    {
                      width: 48,
                      height: 48,
                      position: "absolute",
                      end: 72,
                    },
                  ]}
                >
                  <View
                    style={[
                      commonStyles.bigPickerArrow,
                      { borderTopColor: theme.mainColor },
                    ]}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    if (balance < currentCurrency.max) {
                      setBalance(balance + 500);
                    }
                  }}
                  style={[
                    commonStyles.centeredContent,
                    {
                      width: 48,
                      height: 48,
                      position: "absolute",
                      end: 16,
                    },
                  ]}
                >
                  <View
                    style={[
                      commonStyles.bigPickerArrow,
                      {
                        borderTopColor: theme.mainColor,
                        transform: [{ rotateZ: "180deg" }],
                      },
                    ]}
                  />
                </TouchableOpacity>
              </View>
              <Text
                style={[
                  authStyles.label,
                  {
                    color: currency
                      ? theme.secondColor
                      : COLOR_CONSTANT.darkred,
                  },
                ]}
              >
                {I18n.t("Currency")}
              </Text>
              <Select
                options={accountCurrency}
                value={currency}
                onChange={(itemValue) => setCurrency(itemValue.id)}
              />
              <Text
                style={[
                  authStyles.label,
                  {
                    color: leverage
                      ? theme.secondColor
                      : COLOR_CONSTANT.darkred,
                  },
                ]}
              >
                {I18n.t("Leverage")}
              </Text>
              <Select
                options={accountLeverage}
                value={leverage}
                onChange={(itemValue) => setLeverage(itemValue.id)}
              />
            </View>
            {!!apiError && (
              <Text
                style={[
                  typography.commonText,
                  {
                    marginVertical: 8,
                    color: COLOR_CONSTANT.darkred,
                  },
                ]}
              >
                {apiError}
              </Text>
            )}
            <Row>
              <TouchableOpacity
                onPress={() => handleSubmit()}
                style={[
                  buttonStyles.actionBtn,
                  {
                    backgroundColor: isSubmitDisabled()
                      ? theme.secondBackground_03
                      : COLOR_CONSTANT.green,
                    flex: 1,
                  },
                ]}
                disabled={isSubmitDisabled()}
              >
                <Text style={buttonStyles.actionBtnText}>
                  {I18n.t("Finish").toUpperCase()}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={[
                  buttonStyles.actionBtn,
                  {
                    flex: 1,
                    marginStart: 8,
                    backgroundColor: theme.secondBackground_01,
                  },
                ]}
              >
                <Text style={buttonStyles.actionBtnText}>
                  {I18n.t("Cancel").toUpperCase()}
                </Text>
              </TouchableOpacity>
            </Row>
          </View>
        </Animated.ScrollView>
      )}
    </ThemeContext.Consumer>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ResetDemoAccount);
